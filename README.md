# HTTP Signature Normaliztion
_An HTTP Signatures library that leaves the signing to you_

- [crates.io](https://crates.io/crates/http-signature-normalization)
- [docs.rs](https://docs.rs/http-signature-normalization)
- [Hit me up on Mastodon](https://asonix.dog/@asonix)

Http Signature Normalization is a minimal-dependency crate for producing HTTP Signatures with user-provided signing and verification. The API is simple; there's a series of steps for creation and verification with types that ensure reasonable usage.

```rust
use chrono::Duration;
use http_signature_normalization::Config;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let config = Config {
        expires_after: Duation::secs(5),
    };

    let headers = BTreeMap::new();

    let signature_header_value = config
        .begin_sign("GET", "/foo?bar=baz", headers)
        .sign("my-key-id".to_owned(), |signing_string| {
            // sign the string here
            Ok(signing_string.to_owned()) as Result<_, Box<dyn std::error::Error>>
        })?
        .signature_header();

    let mut headers = BTreeMap::new();
    headers.insert("Signature".to_owned(), signature_header_value);

    let verified = config
        .begin_verify("GET", "/foo?bar=baz", headers)?
        .verify(|sig, signing_string| {
            // Verify the signature here
            sig == signing_string
        });

    assert!(verified)
}
```

### Contributing
Feel free to open issues for anything you find an issue with. Please note that any contributed code will be licensed under the AGPLv3.

### License
Copyright © 2022 Riley Trautman

HTTP Signature Normalization is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

HTTP Signature Normalization is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. This file is part of HTTP Signature Normalization.

You should have received a copy of the GNU General Public License along with HTTP Signature Normalization. If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).
