use base64::prelude::*;
use sha2::{Sha224, Sha256, Sha384, Sha512};

use super::DigestCreate;

fn create<D: sha2::Digest + sha2::digest::FixedOutputReset>(
    digest: &mut D,
    input: &[u8],
) -> String {
    sha2::Digest::update(digest, input);
    BASE64_STANDARD.encode(&digest.finalize_reset())
}

impl DigestCreate for Sha224 {
    const NAME: &'static str = "SHA-224";

    fn compute(&mut self, input: &[u8]) -> String {
        create(self, input)
    }
}

impl DigestCreate for Sha256 {
    const NAME: &'static str = "SHA-256";

    fn compute(&mut self, input: &[u8]) -> String {
        create(self, input)
    }
}

impl DigestCreate for Sha384 {
    const NAME: &'static str = "SHA-384";

    fn compute(&mut self, input: &[u8]) -> String {
        create(self, input)
    }
}

impl DigestCreate for Sha512 {
    const NAME: &'static str = "SHA-512";

    fn compute(&mut self, input: &[u8]) -> String {
        create(self, input)
    }
}
