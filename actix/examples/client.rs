use actix_rt::task::JoinError;
use awc::Client;
use base64::{engine::general_purpose::STANDARD, Engine};
use http_signature_normalization_actix::{digest::ring::Sha256, prelude::*, Canceled};
use tracing::{error, info};
use tracing_error::ErrorLayer;
use tracing_subscriber::{layer::SubscriberExt, EnvFilter};

async fn request(config: Config) -> Result<(), Box<dyn std::error::Error>> {
    let digest = Sha256::new();

    let mut response = Client::default()
        .post("http://127.0.0.1:8010/")
        .append_header(("User-Agent", "Actix Web"))
        .append_header(("Accept", "text/plain"))
        .signature_with_digest(config, "my-key-id", digest, "Hewwo-owo", |s| {
            info!("Signing String\n{}", s);
            Ok(STANDARD.encode(s)) as Result<_, MyError>
        })
        .await?
        .send()
        .await
        .map_err(|e| {
            error!("Error, {}", e);
            MyError::SendRequest
        })?;

    let body = response.body().await.map_err(|e| {
        error!("Error, {}", e);
        MyError::Body
    })?;

    info!("{:?}", body);
    Ok(())
}

#[actix_rt::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let env_filter = EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::new("info"));

    let subscriber = tracing_subscriber::Registry::default()
        .with(env_filter)
        .with(ErrorLayer::default())
        .with(tracing_subscriber::fmt::layer());

    tracing::subscriber::set_global_default(subscriber)?;

    let config = Config::new().require_header("accept").require_digest();

    request(config.clone()).await?;
    request(config.mastodon_compat()).await?;
    Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum MyError {
    #[error("Failed to create signing string, {0}")]
    Convert(#[from] PrepareSignError),

    #[error("Failed to create header, {0}")]
    Header(#[from] InvalidHeaderValue),

    #[error("Failed to send request")]
    SendRequest,

    #[error("Failed to retrieve request body")]
    Body,

    #[error("Blocking operation was canceled")]
    Canceled,
}

impl From<JoinError> for MyError {
    fn from(_: JoinError) -> Self {
        MyError::Canceled
    }
}

impl From<Canceled> for MyError {
    fn from(_: Canceled) -> Self {
        MyError::Canceled
    }
}
