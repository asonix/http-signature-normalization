//! Types for signing requests with Actix Web

use actix_http::header::{HeaderMap, HeaderName, HeaderValue, InvalidHeaderValue, AUTHORIZATION};

/// A thin wrapper around the underlying library's Signed type
///
/// This type can add signatures to Actix Web's HeaderMap
pub struct Signed {
    /// The inner Signed type
    ///
    /// This type can produce Strings representing the Authorization or Signature headers
    pub signed: http_signature_normalization::create::Signed,
}

/// A thin wrapper around the underlying library's Unsigned type
///
/// This is used to prodice the proper Signed type
pub struct Unsigned {
    /// The inner Unsigned type
    pub unsigned: http_signature_normalization::create::Unsigned,
}

impl Signed {
    /// Add the Signature Header to a given HeaderMap
    pub fn signature_header(self, hm: &mut HeaderMap) -> Result<(), InvalidHeaderValue> {
        let sig_header = self.signed.signature_header();
        hm.insert(
            HeaderName::from_static("signature"),
            HeaderValue::from_str(&sig_header)?,
        );

        Ok(())
    }

    /// Add the Authorization Header to a give HeaderMap
    pub fn authorization_header(self, hm: &mut HeaderMap) -> Result<(), InvalidHeaderValue> {
        let auth_header = self.signed.authorization_header();
        hm.insert(AUTHORIZATION, HeaderValue::from_str(&auth_header)?);
        Ok(())
    }
}

impl Unsigned {
    /// Sign the signing_string for the request
    ///
    /// ```rust,ignore
    /// let signed = unsigned.sign("my-key-id".to_owned(), |signing_string| {
    ///     let signature = private_key.sign(signing_string)?;
    ///     Ok(base64::encode(signature))
    /// })?;
    /// ```
    pub fn sign<F, E>(self, key_id: String, f: F) -> Result<Signed, E>
    where
        F: FnOnce(&str) -> Result<String, E>,
    {
        let signed = self.unsigned.sign(key_id, f)?;
        Ok(Signed { signed })
    }
}
