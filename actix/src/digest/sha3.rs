use crate::digest::DigestName;
use sha3::{
    Keccak224, Keccak256, Keccak256Full, Keccak384, Keccak512, Sha3_224, Sha3_256, Sha3_384,
    Sha3_512,
};

impl DigestName for Keccak224 {
    const NAME: &'static str = "keccak-224";
}

impl DigestName for Keccak256 {
    const NAME: &'static str = "keccak-256";
}

impl DigestName for Keccak256Full {
    const NAME: &'static str = "keccak-256-full";
}

impl DigestName for Keccak384 {
    const NAME: &'static str = "keccak-384";
}

impl DigestName for Keccak512 {
    const NAME: &'static str = "keccak-512";
}

impl DigestName for Sha3_224 {
    const NAME: &'static str = "SHA3-224";
}

impl DigestName for Sha3_256 {
    const NAME: &'static str = "SHA3-256";
}

impl DigestName for Sha3_384 {
    const NAME: &'static str = "SHA3-384";
}

impl DigestName for Sha3_512 {
    const NAME: &'static str = "SHA3-512";
}

#[cfg(feature = "client")]
mod client {
    use super::*;
    use crate::digest::DigestCreate;
    use base64::prelude::*;

    fn create<D: sha3::Digest + sha3::digest::FixedOutputReset>(
        digest: &mut D,
        input: &[u8],
    ) -> String {
        sha3::Digest::update(digest, input);
        BASE64_STANDARD.encode(&digest.finalize_reset())
    }

    impl DigestCreate for Sha3_224 {
        fn compute(&mut self, input: &[u8]) -> String {
            create(self, input)
        }
    }

    impl DigestCreate for Sha3_256 {
        fn compute(&mut self, input: &[u8]) -> String {
            create(self, input)
        }
    }

    impl DigestCreate for Sha3_384 {
        fn compute(&mut self, input: &[u8]) -> String {
            create(self, input)
        }
    }

    impl DigestCreate for Sha3_512 {
        fn compute(&mut self, input: &[u8]) -> String {
            create(self, input)
        }
    }

    impl DigestCreate for Keccak224 {
        fn compute(&mut self, input: &[u8]) -> String {
            create(self, input)
        }
    }

    impl DigestCreate for Keccak256 {
        fn compute(&mut self, input: &[u8]) -> String {
            create(self, input)
        }
    }

    impl DigestCreate for Keccak256Full {
        fn compute(&mut self, input: &[u8]) -> String {
            create(self, input)
        }
    }

    impl DigestCreate for Keccak384 {
        fn compute(&mut self, input: &[u8]) -> String {
            create(self, input)
        }
    }

    impl DigestCreate for Keccak512 {
        fn compute(&mut self, input: &[u8]) -> String {
            create(self, input)
        }
    }
}

#[cfg(feature = "server")]
mod server {
    use super::*;
    use crate::digest::{DigestPart, DigestVerify};
    use base64::prelude::*;
    use tracing::{debug, warn};

    fn verify<D: sha3::Digest + sha3::digest::FixedOutputReset>(
        digest: &mut D,
        name: &str,
        parts: &[DigestPart],
    ) -> bool {
        if let Some(part) = parts
            .iter()
            .find(|p| p.algorithm.to_lowercase() == name.to_lowercase())
        {
            debug!("Verifying digest type, {}", name);
            let encoded = BASE64_STANDARD.encode(&digest.finalize_reset());

            return part.digest == encoded;
        }
        warn!("No matching digest algorithm found for {}", name);
        warn!(
            "Provided: [{}]",
            parts.iter().fold(String::new(), |mut acc, item| {
                if acc.is_empty() {
                } else {
                    acc.push_str(", ");
                }
                acc.push_str(&item.algorithm);
                acc
            })
        );

        false
    }

    impl DigestVerify for Sha3_224 {
        fn update(&mut self, part: &[u8]) {
            sha3::Digest::update(self, part);
        }

        fn verify(&mut self, parts: &[DigestPart]) -> bool {
            verify(self, Self::NAME, parts)
        }
    }

    impl DigestVerify for Sha3_256 {
        fn update(&mut self, part: &[u8]) {
            sha3::Digest::update(self, part);
        }

        fn verify(&mut self, parts: &[DigestPart]) -> bool {
            verify(self, Self::NAME, parts)
        }
    }

    impl DigestVerify for Sha3_384 {
        fn update(&mut self, part: &[u8]) {
            sha3::Digest::update(self, part);
        }

        fn verify(&mut self, parts: &[DigestPart]) -> bool {
            verify(self, Self::NAME, parts)
        }
    }

    impl DigestVerify for Sha3_512 {
        fn update(&mut self, part: &[u8]) {
            sha3::Digest::update(self, part);
        }

        fn verify(&mut self, parts: &[DigestPart]) -> bool {
            verify(self, Self::NAME, parts)
        }
    }

    impl DigestVerify for Keccak224 {
        fn update(&mut self, part: &[u8]) {
            sha3::Digest::update(self, part);
        }

        fn verify(&mut self, parts: &[DigestPart]) -> bool {
            verify(self, Self::NAME, parts)
        }
    }

    impl DigestVerify for Keccak256 {
        fn update(&mut self, part: &[u8]) {
            sha3::Digest::update(self, part);
        }

        fn verify(&mut self, parts: &[DigestPart]) -> bool {
            verify(self, Self::NAME, parts)
        }
    }

    impl DigestVerify for Keccak256Full {
        fn update(&mut self, part: &[u8]) {
            sha3::Digest::update(self, part);
        }

        fn verify(&mut self, parts: &[DigestPart]) -> bool {
            verify(self, Self::NAME, parts)
        }
    }

    impl DigestVerify for Keccak384 {
        fn update(&mut self, part: &[u8]) {
            sha3::Digest::update(self, part);
        }

        fn verify(&mut self, parts: &[DigestPart]) -> bool {
            verify(self, Self::NAME, parts)
        }
    }

    impl DigestVerify for Keccak512 {
        fn update(&mut self, part: &[u8]) {
            sha3::Digest::update(self, part);
        }

        fn verify(&mut self, parts: &[DigestPart]) -> bool {
            verify(self, Self::NAME, parts)
        }
    }
}
