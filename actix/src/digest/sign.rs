use actix_http::header::InvalidHeaderValue;
use actix_rt::task::JoinError;
use awc::ClientRequest;
use std::{fmt::Display, future::Future, pin::Pin};

use crate::{
    digest::{DigestClient, DigestCreate, SignExt},
    Config, PrepareSignError, Sign, Spawn,
};

impl SignExt for ClientRequest {
    fn authorization_signature_with_digest<F, E, K, S, D, V>(
        self,
        config: Config<S>,
        key_id: K,
        mut digest: D,
        v: V,
        f: F,
    ) -> Pin<Box<dyn Future<Output = Result<DigestClient<V>, E>>>>
    where
        F: FnOnce(&str) -> Result<String, E> + Send + 'static,
        E: From<JoinError>
            + From<PrepareSignError>
            + From<crate::Canceled>
            + From<InvalidHeaderValue>
            + std::fmt::Debug
            + Send
            + 'static,
        K: Display + 'static,
        S: Spawn + 'static,
        D: DigestCreate + Send + 'static,
        V: AsRef<[u8]> + Send + 'static,
        Self: Sized,
    {
        Box::pin(async move {
            let (d, v) = config
                .spawner
                .spawn_blocking(move || {
                    let d = digest.compute(v.as_ref());
                    Ok((d, v)) as Result<(String, V), E>
                })
                .await??;

            let c = self
                .insert_header(("Digest", format!("{}={}", D::NAME, d)))
                .authorization_signature(config, key_id, f)
                .await?;

            Ok(DigestClient::new(c, v))
        })
    }

    fn signature_with_digest<F, E, K, S, D, V>(
        self,
        config: Config<S>,
        key_id: K,
        mut digest: D,
        v: V,
        f: F,
    ) -> Pin<Box<dyn Future<Output = Result<DigestClient<V>, E>>>>
    where
        F: FnOnce(&str) -> Result<String, E> + Send + 'static,
        E: From<JoinError>
            + From<PrepareSignError>
            + From<crate::Canceled>
            + From<InvalidHeaderValue>
            + std::fmt::Debug
            + Send
            + 'static,
        K: Display + 'static,
        S: Spawn + 'static,
        D: DigestCreate + Send + 'static,
        V: AsRef<[u8]> + Send + 'static,
        Self: Sized,
    {
        Box::pin(async move {
            let (d, v) = config
                .spawner
                .spawn_blocking(move || {
                    let d = digest.compute(v.as_ref());
                    Ok((d, v)) as Result<(String, V), E>
                })
                .await??;

            let c = self
                .insert_header(("Digest", format!("{}={}", D::NAME, d)))
                .signature(config, key_id, f)
                .await?;

            Ok(DigestClient::new(c, v))
        })
    }
}
